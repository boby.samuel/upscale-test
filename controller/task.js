const { taskService } = require("../service")
const { response: { success } } = require("../middleware")

module.exports = {
    async create(req, res, next) {
        try {
            const data = req.body
            const createdTask = await taskService.create(data)
            return success(res, createdTask)
        }
        catch (err) {
            next(err)
        }
    },

    async update(req, res, next) {
        try {
            const data = req.body
            const updatedTask = await taskService.update(req.params.id, data)
            return success(res, updatedTask)
        }
        catch (err) {
            next(err)
        }
    },

    async getOne(req, res, next) {
        try {
            const task = await taskService.getOne(req.params.id)
            return success(res, task)
        }
        catch (err) {
            next(err)
        }
    },

    async getMany(req, res, next) {
        try {
            const { page, limit } = req.query
            const tasks = await taskService.getMany({ page, limit })
            return success(res, tasks)
        }
        catch (err) {
            next(err)
        }
    },

    async delete(req, res, next) {
        try {
            await taskService.delete(req.params.id)
            return success(res, {})
        }
        catch (err) {
            next(err)
        }
    },
}