> pre-requirements: you are expected to have these installed in your machine:
> - nodeJs
> - mongoDb



## **STEP BY STEP GUIDELINE TO RUN THIS SIMPLE REST APPLICATION**

- clone repository to your local directory:
    - go to your desired directory
    - using terminal, paste this `git clone https://gitlab.com/boby.samuel/upscale-test.git`
    - you should have a folder named **upscale-test**

- change directory to your new cloned directory, `cd upscale-test`
- execute `npm install`. This command is to download all necessary packages for the application
- to run your application, execute `nodemon`. 
- the application shold be running on port **3000**


## API List
since you're running the app locally, therefore the url is **localhost:3000**.

Postman documentation:
**https://api.postman.com/collections/6701019-5d73d409-11ed-4647-8bc1-37973618cb56?access_key=PMAT-01H2830W159WGC8MHZBHQKGZA6**

<details><summary>GET /tasks?page=<number>&limit=<number></summary>
    get all tasks, with pagination
    page and number expected to be integer
</details>

<details><summary>GET /task/:id</summary>
    get one task. id is mongoId
</details>

<details><summary>PATCH /task/:id</summary>
    id = mongoId
    expected payload:
    `
        {
            title: "your new title",
            description: "your new description"
        }
    `
</details>

<details><summary>POST /task</summary>
    create a task.
    expected payload:
    `
        {
            title: "your new title",
            description: "your new description"
        }
    `
    title is required
</details>

<details><summary>DELETE /task/:id</summary>
    delete one task. id is mongoId
</details>
