const express = require('express')
const router = express.Router()
const { taskController } = require("../controller")
const routeConfig = (method, path, controller) => ({
    method, path, controller
})

const routeCollections = [
    routeConfig("get", "/tasks", taskController.getMany),
    routeConfig("get", "/task/:id", taskController.getOne),
    routeConfig("post", "/task", taskController.create),
    routeConfig("patch", "task/:id", taskController.update),
    routeConfig("delete", "/task/:id", taskController.delete),
    // new routes will be stored here
]

routeCollections.forEach(({ method, path, controller }) => {
    router[method](path, controller)
})

module.exports = router