const express = require('express')
const router = express.Router()
const routers = {
    taskRouter: require("./task"),
    // new routes will be stored here
}

Object.values(routers).forEach(route => {
    router.use(route)
})

module.exports = router