const { taskModel } = require("../model")

module.exports = {
    async create(payload) {
        try {
            return await taskModel.create(payload)
        }
        catch (err) {
            throw err
        }
    },

    async update(id, payload) {
        try {
            return await taskModel.findByIdAndUpdate(id, payload, { new: true })
        }
        catch (err) {
            throw err
        }
    },

    async getOne(id) {
        try {
            return await taskModel.findById(id)
        }
        catch (err) {
            throw err
        }
    },

    async getMany({ page, limit }) {
        try {
            const skip = (page - 1) * limit
            const tasks = await taskModel.find({})
                .limit(limit)
                .skip(skip)
                .sort({ _id: "desc" })
            return tasks
        }
        catch (err) {
            throw err
        }
    },

    async delete(id) {
        try {
            return await taskModel.findByIdAndDelete(id)
        }
        catch (err) {
            throw err
        }
    }
}