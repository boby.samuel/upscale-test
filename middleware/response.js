module.exports = {
    success(res, data) {
        res.status(200).json({
            success: true,
            data
        })
    },
    failed(res, message) {
        res.status(400).json({
            success: false,
            message
        })
    }
}