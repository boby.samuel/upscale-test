exports._404 = (req, res, next) => {
    return res.status(404).json({
        success: false,
        message: "route not found"
    })
}

exports.errorHandler = (err, req, res, next) => {
    if (err) {
        return res.status(400).json({
            success: false,
            message: err.message
        })
    }

    next()
}