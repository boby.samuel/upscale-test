const express = require('express')
const app = express()
require('dotenv').config()
const cookieParser = require("cookie-parser")
const cors = require('cors')
const { errorHandler: { _404, errorHandler } } = require("./middleware")
const mainRoute = require("./route")

// mongoDB connection setup
const mongoose = require('mongoose');
const DB_CONNECTION_STRING = "mongodb://localhost:27017"
mongoose.connect(DB_CONNECTION_STRING, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// CORS Setting
const corsOptions = {
    origin: '*',
    allowedHeaders: ['Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With', 'Accept'],
    methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

// elements represent priority
const startupSequence = [
    cors(corsOptions),
    express.json({ limit: '25mb' }),
    express.urlencoded({ extended: true, limit: '25mb' }),
    cookieParser(),
    mainRoute,
    errorHandler,
]

startupSequence.forEach(action => {
    app.use(action)
})

// app.get('/', (req, res) => res.status(200).json({
//     success: true,
//     data: "welcome"
// }))

// app.use(require("./route"))


// app.use(errorHandler)
app.use("*", _404)
app.listen(3000, () => console.log('running on port 3000'))
module.exports = app